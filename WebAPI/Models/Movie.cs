﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public  class Movie
    {
        public int MovieId { get; set; }
        [MaxLength(50)] 
        public string Title { get; set; }
        [MaxLength(20)]
        public string? Genre { get; set; }
        public int ReleaseYear { get; set; }
        [MaxLength(20)]
        public string? Director { get; set; }
        public string? PictureURL { get; set; }
        public string? TrailerLink { get; set; }

        //one movie has many characters and characters can play in multiple movies

        public int FranchiseId { get; set; }
        public int CharacterId { get; set; }
        public ICollection<Character> Characters{ get; set; }
        
        
        
    }
}
