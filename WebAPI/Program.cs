﻿// See https://aka.ms/new-console-template for more information
using Microsoft.EntityFrameworkCore;
using WebAPI.Data;
using WebAPI.Models;





/// <summary>
/// This method creates the character Thor for the movie Thor on every call
/// </summary>
static void InsertingNamedInstance()
{
    
    Character thor = new Character()
    {
       FullName = "Thor Ragnarog",
        Alias = "Thor Odinson ",
        Gender = "Male"
      
    };
    using (MovieDb context = new MovieDb())
    {
        context.Characters.Add(thor);
        context.SaveChanges();
    }
}





