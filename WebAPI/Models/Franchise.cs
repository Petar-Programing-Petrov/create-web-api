﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class Franchise
    {
        public int FranchiseId { get; set; }
        [MaxLength(50)]
        public string FranchiseName { get; set; }
        [MaxLength(500)]
        public string?  FranchiseDescription { get; set; }

        
        public ICollection<Movie> Movies { get; set; }
        
    }
}
