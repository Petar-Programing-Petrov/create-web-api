﻿using Microsoft.EntityFrameworkCore;
using WebAPI.Models;

namespace WebAPI.Data
{
    public class MovieDb :DbContext
    {
        //Creating Tables
        public DbSet<Character> Characters { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source= GAMING\\SQLEXPRESS; Initial Catalog = MovieDb; Integrated Security = True;"); 
        }
        //Seeding Data
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            // Movie seeding
             modelBuilder.Entity<Movie>()
                .HasData(new Movie() { MovieId = 1, FranchiseId = 1, Genre = "Action", ReleaseYear = 1992, Title = "The Amazing Spider-Man 2" , CharacterId = 2 });
            modelBuilder.Entity<Movie>()
                .HasData(new Movie() { MovieId = 2, FranchiseId = 2, Genre = "Comedy", ReleaseYear = 1996, Title = "The  SpiNder-Man 31", CharacterId = 1 });
            modelBuilder.Entity<Movie>()
                .HasData(new Movie() { MovieId = 3, FranchiseId = 1, Genre = "Action", ReleaseYear = 2011, Title = "Thor", Director = "Kenneth Branagh", CharacterId = 4 });
            modelBuilder.Entity<Movie>()
                .HasData(new Movie() { MovieId = 4, FranchiseId = 1, Genre = "Action", ReleaseYear = 2008, Title = "Iron Man", Director = "Jon Favreau", CharacterId = 5 });
            modelBuilder.Entity<Movie>()
               .HasData(new Movie() { MovieId = 5, FranchiseId = 1, Genre = "Action", ReleaseYear = 2018, Title = "Iron Man 2", Director = "Jon Favreau", CharacterId = 6 });

            //Franchise seeding
            modelBuilder.Entity<Franchise>()
                .HasData(new Franchise() { FranchiseId = 1, FranchiseName = "Marvel Cinematic Universe"});            
            modelBuilder.Entity<Franchise>()
                .HasData(new Franchise() { FranchiseId = 2, FranchiseName = "Marvel Comedy Unity"});

            //Characters seeding
            modelBuilder.Entity<Character>()
                .HasData(new Character() { CharacterId = 1, FullName = "SpiNder-Man", Alias = "Pioter Pirker", Gender = "Male"});
            modelBuilder.Entity<Character>()
               .HasData(new Character() { CharacterId = 2, FullName = "SpiderReal-Man", Alias = "Piter Parker", Gender = "Male"});
            modelBuilder.Entity<Character>()
               .HasData(new Character() { CharacterId = 3, FullName = "Thanos", Alias = "MR.Been", Gender = "Male",});
            modelBuilder.Entity<Character>()
               .HasData(new Character() { CharacterId = 4, FullName = "ThorSeededMigration", Alias = "Dean", Gender = "Male"});
            modelBuilder.Entity<Character>()
            .HasData(new Character() { CharacterId = 5, FullName = "Iron-Mann", Alias = "Vilyana", Gender = "Female"});
            modelBuilder.Entity<Character>()
            .HasData(new Character() { CharacterId = 6, FullName = "Falkon", Alias = "Petar", Gender = "Male"});
        }
    }
}
