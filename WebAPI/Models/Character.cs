﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class Character
    {
        public int CharacterId { get; set; }
        [MaxLength(50)]
        public string FullName { get; set; }
        [MaxLength(50)]        
        public string Alias { get; set; }
        [MaxLength(10)]
        public string Gender { get; set; }
        public string? PictureURL { get; set; }

        //Reference to many movies
       
        
        public ICollection<Movie> Movies { get; set; }
    }
}
