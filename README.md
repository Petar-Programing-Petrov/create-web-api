## Name

Web API Creation and documentation

## Description
An entity framework code first workflow and an ASP.NET core web API in C#.


## Visuals
Will add an SSMS diagram

## Installation
Visual Studio 2022 and .NET 6
SQL Server Management Studio
Microsoft Entity FrameworkCore SqlServer
Microsoft Entity FrameworkCore Design
Microsoft Entity FrameworkCore Tools

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
For support please contact the authors of this project.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
angelova24 and Petar-Programming-Petrov

## Authors and acknowledgment
angelova24 and Petar-Programming-Petrov

## License
For open source projects, say how it is licensed.

## Project status
Work in Progress
